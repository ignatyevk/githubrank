import sbt._

object Dependencies {

  object ZIO {
    val zioVersion = "1.0.12"
    val interopVersion = "2.3.1.0"
    val loggingVersion = "0.5.13"

    val zio2 = "org.scalaz" % "scalaz-zio_2.12" % "0.6.0"
    val zio = "dev.zio" %% "zio" % zioVersion
    val macros = "dev.zio" %% "zio-macros" % zioVersion
    val catsInterop = "dev.zio" %% "zio-interop-cats" % interopVersion
    val logging = "dev.zio" %% "zio-logging" % loggingVersion
    val loggingSlf4j = "dev.zio" %% "zio-logging-slf4j" % loggingVersion
    val cache = "com.github.blemale" % "scaffeine_2.13" % "5.1.2"
    val all = Seq(zio, macros, cache, zio2, catsInterop, logging, loggingSlf4j)
  }

  object Cats {
    private val catsV = "2.2.0"
    private val catsEffectV = "2.2.0"

    val core = "org.typelevel" %% "cats-core" % catsV
    val effect = "org.typelevel" %% "cats-effect" % catsEffectV

    val all = Seq(core, effect)
  }

  object Tapir {
    private val version = "0.17.19"

    val http4s = "com.softwaremill.sttp.tapir" % "tapir-zio-http4s-server_2.13" % version
    val zioHttp4sServer = "com.softwaremill.sttp.tapir" % "tapir-zio-http4s-server_2.13" % version
    val zio = "com.softwaremill.sttp.tapir" % "tapir-zio_2.13" % version
    val openApiCirceYaml = "com.softwaremill.sttp.tapir" % "tapir-openapi-circe-yaml_2.13" % version
    val docs = "com.softwaremill.sttp.tapir" % "tapir-openapi-docs_2.13" % version
    val circe = "com.softwaremill.sttp.tapir" % "tapir-json-circe_2.13" % version
    val swagger = "com.softwaremill.sttp.tapir" % "tapir-swagger-ui-http4s_2.13" % version

    val all = Seq(openApiCirceYaml, http4s, zio, zioHttp4sServer, circe, swagger, docs)
  }

  object Http4s {
    private val version = "0.21.6"

    val server = "org.http4s" %% "http4s-blaze-server" % version
    val dsl = "org.http4s" %% "http4s-dsl" % version
    val core = "org.http4s" %% "http4s-core" % version
    val client = "org.http4s" %% "http4s-blaze-client" % version
    val circe = "org.http4s" %% "http4s-circe" % version

    val all = Seq(server, dsl, client, circe, core /*,prometheus, prometheusHtt4s*/)
  }

  object Circe {
    private val version = "0.13.0"

    val core = "io.circe" %% "circe-core" % version
    val literal = "io.circe" % "circe-literal_2.13" % version
    val generic = "io.circe" %% "circe-generic" % version
    val parser = "io.circe" %% "circe-parser" % version
    val refined = "io.circe" %% "circe-refined" % "0.13.0"

    val all = Seq(core, generic, parser, refined, literal)
  }

  object Config {
    private val version = "0.14.1"
    private val refinedV = "0.9.22"

    val pureconfig = "com.github.pureconfig" %% "pureconfig" % version
    val pureconfigRefined = "eu.timepit" %% "refined" % refinedV

    val all = Seq(pureconfig, pureconfigRefined)
  }

  object Logging {
    private val logbackV = "1.2.3"
    val typesafeLog = "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
    val Core = "ch.qos.logback" % "logback-core" % logbackV
    val Logback = "ch.qos.logback" % "logback-classic" % logbackV
    val all = Seq(Core, Logback, typesafeLog)
  }

  object Testing {
    val zioTest = "dev.zio" %% "zio-test" % ZIO.zioVersion
    val zioTestSbt = "dev.zio" %% "zio-test-sbt" % ZIO.zioVersion
    val all = Seq(zioTest, zioTestSbt)
  }
}