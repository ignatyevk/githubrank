package com.ignatyevka.githubrank

import cats.effect.{ExitCode => CatsExitCode}
import com.ignatyevka.githubrank.http.routes.routes
import org.http4s.implicits.http4sKleisliResponseSyntaxOptionT
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import zio.ZIO.runtime
import zio.console.putStrLn
import zio.interop.catz._
import zio.logging.log
import zio.{URIO, ZIO, _}

object Main extends App {
  override def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    val program: ZIO[AppEnv, Throwable, Unit] =
      for {
        conf <- config.appConfig
        _ <- log.info(s"Swagger started at http://" + conf.api.host + ":" + conf.api.port + "/githubrank/docs")
        server <- runtime[AppEnv].flatMap { implicit rts =>
          BlazeServerBuilder.apply[AppTask](rts.platform.executor.asEC)
            .bindHttp(conf.api.port, conf.api.host)
            .withHttpApp(Router("/githubrank" -> routes).orNotFound)
            .serve
            .compile[AppTask, AppTask, CatsExitCode]
            .drain
        }.orDie
      } yield server

    program
      .provideSomeLayer[ZEnv](appEnvironment)
      .tapError(err => putStrLn(s"Execution failed with: $err"))
      .exitCode
  }
}
