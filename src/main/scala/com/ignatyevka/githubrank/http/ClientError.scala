package com.ignatyevka.githubrank.http

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

sealed trait ClientError extends Product with Serializable

object ClientError {

  final case class GitHubResponseError(message: String) extends Throwable("github response error: " + message)

  object GitHubResponseError {
    implicit val codec: Codec[GitHubResponseError] = deriveCodec
  }
}