package com.ignatyevka.githubrank.http

import com.ignatyevka.githubrank.config.Config
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import zio.interop.catz._
import zio.{Has, Task, URIO, ZIO, ZLayer, ZManaged}

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits

object HClient {

  trait Service {
    def client: Client[Task]
  }

  type HClient = Has[Service]

  case class SimpleClient(client: Client[Task]) extends Service

  def clientManaged: ZManaged[Any, Throwable, Client[Task]] = {
    ZIO.runtime[Any].map { implicit rts =>
      BlazeClientBuilder
        .apply[Task](ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10)))
        .resource
        .toManaged
    }.toManaged_.flatten
  }

  val life: ZLayer[Config, Throwable, HClient] = ZLayer.fromManaged(for {
    client <- clientManaged.map(x => SimpleClient(x))
  } yield client)

  def client: URIO[HClient, Client[Task]] = ZIO.access[HClient](_.get.client)
}