package com.ignatyevka.githubrank.http.routes

import com.ignatyevka.githubrank.data.AggregateContributor
import com.ignatyevka.githubrank.service.CacheService.{CachingContent, CachingEtag}
import com.ignatyevka.githubrank.service.GitHubRankService
import com.ignatyevka.githubrank.service.GitHubRankService.GithubRankService
import io.circe.literal._
import io.circe.{Decoder, Encoder}
import org.http4s._
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.dsl.Http4sDsl
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.ztapir._
import zio.RIO
import zio.interop.catz._
import zio.logging.{Logging, log}

final case class GitHubRankRoutes[R <: GithubRankService with Logging  with CachingContent with CachingEtag]() {
  type GithubRankTask[A] = RIO[R, A]

  private val dsl: Http4sDsl[GithubRankTask] = Http4sDsl[GithubRankTask]

  implicit def confirmJsonDecoder[A](implicit decoder: Decoder[A]): EntityDecoder[GithubRankTask, A] = jsonOf[GithubRankTask, A]

  implicit def confirmJsonEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[GithubRankTask, A] = jsonEncoderOf[GithubRankTask, A]

  import dsl._

  val githubRank: HttpRoutes[GithubRankTask] = HttpRoutes.of[GithubRankTask] {
    case GET -> Root / "org" / org_name / "contributors" :? PageQueryParamMatcher(page) +& per_pageQueryParamMatcher(per_page) =>
      GitHubRankService.getSortedContributorsByOrg(org_name, page.getOrElse(1), per_page.getOrElse(30))
        .foldM(
          err => log.info(err.getMessage) *> BadRequest(err.getMessage),
          resp => log.info("sorted contributors " + resp) *> Ok(resp))
  }
}

/**
 * swagger spec
 */
object GitHubRankRoutes {
  val getSortedRank = endpoint
    .in("githubrank")
    .tag("githubrank")
    .description("return a list sorted by the number of contributions made by the developer to all repositories for" +
      " the given organization.")
    .in(
      "org"
        / path[String]("org_mame")
        / "contributors"
        / query[Option[Int]]("page").default(Some(1)).and(
        query[Option[Int]]("per_page").default(Some(30))))
    .get
    .out(jsonBody[List[AggregateContributor]].example(List(AggregateContributor.test)))
}