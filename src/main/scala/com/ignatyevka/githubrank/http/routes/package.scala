package com.ignatyevka.githubrank.http

import cats.implicits._
import com.ignatyevka.githubrank.AppEnv
import com.ignatyevka.githubrank.http.routes.GitHubRankRoutes.getSortedRank
import org.http4s.dsl.impl.OptionalQueryParamDecoderMatcher
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.swagger.http4s.SwaggerHttp4s
import zio.interop.catz._

package object routes {
  val githubRankRoutes = GitHubRankRoutes[AppEnv]()

  def routes =  githubRankRoutes.githubRank <+> new SwaggerHttp4s(yaml).routes

  lazy val githubRankEndpoint = Seq(getSortedRank)

  lazy val applicationEndpoints = githubRankEndpoint

  lazy val openApi = OpenAPIDocsInterpreter.toOpenAPI(applicationEndpoints, "githubrank", "1.0")

  lazy val yaml: String = openApi.toYaml

  object PageQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Int]("page")

  object per_pageQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Int]("per_page")

}

