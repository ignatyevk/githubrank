package com.ignatyevka.githubrank

import zio.logging.Logging
import zio.{Has, Task, URIO, ZIO, ZLayer}

import scala.concurrent.duration.{FiniteDuration, _}

package object config {

  type Config =  Has[AppConfig]

  final case class AppConfig(api: ApiConfig, github: GithubConfig)

  final case class GithubConfig(ghToken: Option[String], ghUrl:String)

  final case class ApiConfig(
                              host: String,
                              port: Int,
                              poolSize: Int = ApiConfig.defaultPoolSize,
                              responseHeaderTimeout: FiniteDuration = 10.seconds,
                              idleTimeout: FiniteDuration = 30.seconds)

  object ApiConfig {
    private val cores = Runtime.getRuntime.availableProcessors()
    val defaultPoolSize: Int = math.max(4, cores + 1)
  }

  val appConfig: URIO[Has[AppConfig], AppConfig] = ZIO.service

  object Configuration {

    import pureconfig._
    import pureconfig.generic.auto._

  val live: ZLayer[Logging, Throwable, Config] = Task.effect(ConfigSource.default.loadOrThrow[AppConfig]).map(Has(_)).toLayerMany
}
}