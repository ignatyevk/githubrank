package com.ignatyevka

import com.ignatyevka.githubrank.config.{Config, Configuration}
import com.ignatyevka.githubrank.http.HClient
import com.ignatyevka.githubrank.http.HClient.HClient
import com.ignatyevka.githubrank.service.CacheService.{CachingContent, CachingEtag}
import com.ignatyevka.githubrank.service.GitHubRankService.GithubRankService
import com.ignatyevka.githubrank.service.{CacheService, githubRankServiceEnv}
import zio.RIO
import zio.blocking.Blocking
import zio.clock.Clock
import zio.logging.Logging

package object githubrank {
  type AppEnv =
    Logging with
      Config with
      Blocking with
      Clock with
      HClient with
      GithubRankService with
      CachingEtag with
    CachingContent

  type AppTask[A] = RIO[AppEnv, A]

  val confEnv = Configuration.live
  val client = HClient.life

  val appEnvironment =
    Logger.live >+>
      confEnv >+>
      Blocking.live >+>
      client >+>
      githubRankServiceEnv >+>
      CacheService.lifeEtagCache >+>
      CacheService.lifeContentCache
}