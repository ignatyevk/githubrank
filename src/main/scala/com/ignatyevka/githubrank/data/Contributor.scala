package com.ignatyevka.githubrank.data

import io.circe.generic.JsonCodec

@JsonCodec
case class Contributor(login: String, contributions: Int)

object Contributor {
  val test = Contributor("testContributorName", 5)
  val test2 = Contributor("testContributorName2", 10)
  val test3 = Contributor("testContributorName2", 30)
  val test4 = Contributor("testContributorName3", 15)
}