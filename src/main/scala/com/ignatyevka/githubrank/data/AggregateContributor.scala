package com.ignatyevka.githubrank.data

import io.circe.generic.JsonCodec

@JsonCodec
case class AggregateContributor(name: String, contributions: Int)

object AggregateContributor {
  val test = AggregateContributor("testContributorName", 5)
  val test2 = AggregateContributor("testContributorName2", 10)
  val test3 = AggregateContributor("testContributorName2", 20)
  val test4 = AggregateContributor("testContributorName3", 15)
}