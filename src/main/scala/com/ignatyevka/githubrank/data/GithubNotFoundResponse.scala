package com.ignatyevka.githubrank.data

import io.circe.generic.JsonCodec

@JsonCodec
case class GithubNotFoundResponse(message: String, documentation_url: String)

object GithubNotFoundResponse {
  val test = GithubNotFoundResponse("notFound", "test")
}