package com.ignatyevka.githubrank.data

import io.circe.generic.JsonCodec

@JsonCodec
case class OrgRepositories(contributors_url: String)

object OrgRepositories {
  val test = OrgRepositories("http://test")
  val test2 = OrgRepositories("http://test")
}
