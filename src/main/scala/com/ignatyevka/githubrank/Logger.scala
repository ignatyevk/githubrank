package com.ignatyevka.githubrank

import zio.URLayer
import zio.clock.Clock
import zio.console.Console
import zio.logging.{LogLevel, Logging}
object Logger {

  val live = Logging.console(logLevel = LogLevel.Info) >>> Logging.withRootLoggerName("githubRank")

  val test:URLayer[Console with Clock,Logging] = Logging.console()

}