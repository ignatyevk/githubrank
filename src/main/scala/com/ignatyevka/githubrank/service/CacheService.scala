package com.ignatyevka.githubrank.service

import com.github.blemale.scaffeine.{Cache, Scaffeine}
import org.http4s.Response
import zio.{Has, IO, Task, URIO, ZIO, ZLayer}

import scala.concurrent.duration.DurationInt

object CacheService {
  type CachingEtag = Has[Cache[String, String]]
  type CachingContent = Has[Cache[String,  Response[Task]]]
  val etagCache: URIO[Has[Cache[String, String]], Cache[String, String]] = ZIO.service
  val contentCache: URIO[Has[Cache[String, Response[Task]]], Cache[String, Response[Task]]] = ZIO.service

  val lifeEtagCache: ZLayer[Any, Throwable, Has[Cache[String, String]]] =
    Task(Scaffeine()
      .recordStats()
      .expireAfterWrite(1.hour)
      .maximumSize(5000)
      .build[String, String]()).toLayer

  val lifeContentCache: ZLayer[Any, Throwable, Has[Cache[String, Response[Task]]]] =
    Task(Scaffeine()
      .recordStats()
      .expireAfterWrite(1.hour)
      .maximumSize(5000)
      .build[String, Response[Task]]()).toLayer
}