package com.ignatyevka.githubrank

import com.ignatyevka.githubrank.config.Configuration
import com.ignatyevka.githubrank.data.{Contributor, GithubNotFoundResponse, OrgRepositories}
import io.circe.Decoder
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}
import zio.Task
import zio.interop.catz._

package object service {
  implicit def orgReposDecoder(implicit decoder: Decoder[OrgRepositories]):EntityDecoder[Task,OrgRepositories] = jsonOf[Task, OrgRepositories]
  implicit def notFoundDecoder(implicit decoder: Decoder[GithubNotFoundResponse]):EntityDecoder[Task,GithubNotFoundResponse] = jsonOf[Task, GithubNotFoundResponse]
  implicit def orgReposDecoderL(implicit decoder: Decoder[List[OrgRepositories]]):EntityDecoder[Task,List[OrgRepositories]] = jsonOf[Task, List[OrgRepositories]]
  implicit def contributorsDecoderL(implicit decoder: Decoder[List[Contributor]]):EntityDecoder[Task,List[Contributor]] = jsonOf[Task, List[Contributor]]
  implicit def contributorsDecoder(implicit decoder: Decoder[Contributor]):EntityDecoder[Task,Contributor] = jsonOf[Task, Contributor]
  implicit def orgReposEncoder: EntityEncoder[Task, Map[String, String]] = jsonEncoderOf[Task, Map[String,String]]

  val githubRankServiceEnv = (GitHubClient.life  ++ Configuration.live) >>>  GitHubRankService.life
}