package com.ignatyevka.githubrank.service

import cats.implicits._
import com.ignatyevka.githubrank.data.{AggregateContributor, Contributor}
import com.ignatyevka.githubrank.service.CacheService.{CachingContent, CachingEtag}
import com.ignatyevka.githubrank.service.GitHubClient.GithubClient
import zio.logging.Logging
import zio.macros.accessible
import zio.{Has, RIO, URLayer, ZIO, ZLayer}

@accessible
object GitHubRankService {
  type GithubRankService = Has[Service]

  trait Service {
    /**
     * @return first: gitHubClient return list of contributors_url for each repositories for given organisation name
     *         second: gitHubClient return contributors for each repos by contributors_url (parallel)
     *         third: aggregate and prepared lists of contributors
     */
    def getSortedContributorsByOrg(org_name: String, page: Int, per_page: Int): RIO[Logging with CachingContent with CachingEtag, List[AggregateContributor]]
  }

  val life: URLayer[GithubClient, GithubRankService] = ZLayer.fromService[GitHubClient.Service, Service] { githubClient =>
    (org_name: String, page: Int, per_page: Int) => {
      /**
       * @param contributors list of contributors of all repositories
       * @return aggregate by name, sort and return list with length equals page sise
       */
      def mergeAndSortContributors(contributors: List[List[Contributor]]): List[AggregateContributor] =
        contributors
          .flatten
          .groupBy(_.login)
          .view
          .mapValues(contributors => contributors.map(_.contributions).sum)
          .toMap
          .map { case (k, v) => AggregateContributor(k, v) }
          .toList
          .sortBy(_.contributions)
          .reverse
          .take(if (per_page >= 0) per_page else 30)

      for {
        orgRepos <- githubClient.getRepositories(org_name, page, per_page)
        sortedContributors <- ZIO.foreachPar(orgRepos.map(_.contributors_url)) {
          githubClient.getContributors(_, page, per_page)
        }.map(mergeAndSortContributors)
      } yield sortedContributors
    }
  }
}