package com.ignatyevka.githubrank.service

import com.ignatyevka.githubrank.config
import com.ignatyevka.githubrank.config.AppConfig
import com.ignatyevka.githubrank.data.{Contributor, OrgRepositories}
import com.ignatyevka.githubrank.http.ClientError.GitHubResponseError
import com.ignatyevka.githubrank.http.HClient
import com.ignatyevka.githubrank.http.HClient.HClient
import com.ignatyevka.githubrank.service.CacheService.{CachingContent, CachingEtag, contentCache, etagCache}
import org.http4s._
import org.http4s.client.Client
import org.http4s.headers.ETag
import zio.interop.catz._
import zio.logging.{Logging, log}
import zio.macros.accessible
import zio.{ZIO, _}

@accessible
object GitHubClient {
  type GithubClient = Has[Service]

  trait Service {
    def getRepositories(org_name: String, page: Int, per_page: Int): RIO[CachingEtag with CachingContent with Logging, List[OrgRepositories]]

    def getContributors(contributors_url: String, page: Int, per_page: Int): RIO[CachingEtag with CachingContent with Logging, List[Contributor]]
  }

  val life: ZLayer[Has[AppConfig] with HClient, Throwable, GithubClient] =
    ZLayer.fromManaged(for {
      client <- HClient.client.toManaged_
      conf <- config.appConfig.toManaged_
    } yield new GitHubClient(client, conf))
}

final case class GitHubClient(client: Client[Task], config: AppConfig) extends GitHubClient.Service {

  /**
   *
   * @param org_name organization name
   * @param page     number of page
   * @param per_page count of row per page
   * @return pageble organization repositories with contributors url
   */
  override def getRepositories(org_name: String, page: Int, per_page: Int): RIO[CachingEtag with CachingContent with Logging, List[OrgRepositories]] = {
    for {

      r <- makeGitHubRequest[OrgRepositories](
        Uri.unsafeFromString(config.github.ghUrl)./("orgs")./(org_name)./("repos"),
        page,
        per_page,
        Task.fail(GitHubResponseError("organisation by " + org_name + " not found")))
    } yield r

  }

  /**
   *
   * @param contributors_url by this url we take all contributors for this repo
   * @param page             number of page
   * @param per_page         count of row per page
   * @return pageble contributors for repository
   */
  override def getContributors(contributors_url: String, page: Int, per_page: Int): RIO[CachingEtag with CachingContent with Logging, List[Contributor]] =
    makeGitHubRequest[Contributor](Uri.unsafeFromString(contributors_url), page, per_page, UIO(List.empty[Contributor]))

  /**
   * Note: in case where request with contributors_url retrieve 204 or 404 return empty list.
   * in case where request with org_name retrieve 404 throw error
   *
   * If env variable GH_TOKEN exist set it to headers for auth for improve rate limit
   *
   * also caching responses with ETag logic
   *
   * @param uri              uri for request
   * @param page             number of page
   * @param per_page         count of row per page
   * @param notFoundBehavior effect that must invoke where gitHub api retrieve 404 status
   * @param enc              encoder
   * @tparam A expected type
   * @return list of entity with type 'A'
   */
  private def makeGitHubRequest[A](
                                    uri: Uri,
                                    page: Int,
                                    per_page: Int,
                                    notFoundBehavior: Task[List[A]])(implicit enc: EntityDecoder[Task, List[A]]):
  ZIO[Logging with CachingEtag with CachingContent, Throwable, List[A]] = {

    val request = Request[Task](Method.GET,
      uri
        .withQueryParam("page", page)
        .withQueryParam("per_page", per_page))
      .withHeaders(Header.apply("Accept", "application/vnd.github.v3+json"))

    val cacheKey = uri.toString() + "page:" +page + "per+page" + per_page

    for {
      mbEtag <- etagCache.map(x => x.getIfPresent(cacheKey))

      result <- mbEtag.fold(client.run(maybeWithToken(request)))(etag => {
        client.run(withEtag(etag, maybeWithToken(request)))})
        .use(ghResponse => {
          val newETag = ghResponse.headers.get(ETag.name).map(_.value).getOrElse("")
          ghResponse.status match {
            case Status.NoContent => Task(Nil)
            case Status.NotModified => contentCache.map(_.getIfPresent(mbEtag.getOrElse("")).get).flatMap(_.as[List[A]])
            case Status.NotFound => notFoundBehavior
            case _ => contentCache.map(_.invalidate(mbEtag.getOrElse(""))) *> contentCache.map(_.put(newETag, ghResponse)) *>
              etagCache.map(_.invalidate(cacheKey)) *> etagCache.map(_.put(cacheKey, prepareETag(newETag))) *> ghResponse.as[List[A]]
          }
        }).foldM(
        err => log.info(GitHubResponseError(err.getMessage).getMessage) *> IO.fail(err),
        maybeResponse => log.info("github response: " + maybeResponse).as(maybeResponse))
    } yield result
  }

  private def maybeWithToken(r: Request[Task]): Request[Task] = config.github.ghToken.fold(r)(x =>
    r.withHeaders(Header.apply("Authorization", "token " + x)))

  private def prepareETag(etag: String): String = etag.replaceFirst("W/", "").replaceAll("\"", "")

  private def withEtag(etag: String, r: Request[Task]): Request[Task] = r.withHeaders(Header.apply("If-None-Match", etag))
}