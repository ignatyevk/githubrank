package com.ignatyevka.githubrank

import com.ignatyevka.githubrank.data.{AggregateContributor, Contributor, OrgRepositories}
import com.ignatyevka.githubrank.http.ClientError.GitHubResponseError
import com.ignatyevka.githubrank.service.{CacheService, GitHubClient, GitHubRankService}
import zio.test.Assertion.{equalTo, fails}
import zio.test._
import zio.test.mock.Expectation.{failure, value}
import zio.test.mock.mockable


@mockable[GitHubRankService.Service]
object GitHubRankMock

@mockable[GitHubClient.Service]
object GitHubClientMock

object GitHubRankServiceSpec extends DefaultRunnableSpec {

  override def spec: ZSpec[Environment, Failure] = suite("LiveSubscriptionLogicSpec")(

    testM("successfully return single contributor for single repo") {
      checkM(Gen.anyString) { org_name =>
        val repositories = List(OrgRepositories.test)

        val contributors = List(Contributor.test)

        val gitHubClientMocks = (CacheService.lifeContentCache ++ CacheService.lifeEtagCache) >+> (
          GitHubClientMock.GetRepositories(equalTo(org_name, 1, 2), value(repositories)) ++
            GitHubClientMock.GetContributors(equalTo(OrgRepositories.test.contributors_url, 1, 2), value(contributors))
          ).toLayer

        val layer = (Logger.test ++ gitHubClientMocks) >+> GitHubRankService.life

        assertM(GitHubRankService.getSortedContributorsByOrg(org_name, 1, 2))(equalTo(List(AggregateContributor.test))).provideLayer(layer)

      }
    },
    testM("successfully aggregate two different contributors for single repo") {
      checkM(Gen.anyString) { org_name =>

        val repositories = List(OrgRepositories.test, OrgRepositories.test)

        val contributors = List(Contributor.test)
        val contributors2 = List(Contributor.test2)

        val gitHubClientMocks = (CacheService.lifeContentCache ++ CacheService.lifeEtagCache) >+> (
          GitHubClientMock.GetRepositories(equalTo(org_name, 1, 30), value(repositories)) ++
            GitHubClientMock.GetContributors(equalTo(OrgRepositories.test2.contributors_url, 1, 30), value(contributors2)) &&
            GitHubClientMock.GetContributors(equalTo(OrgRepositories.test.contributors_url, 1, 30), value(contributors))
          ).toLayer

        val layer = (Logger.test ++ gitHubClientMocks) >+> GitHubRankService.life

        assertM(GitHubRankService.getSortedContributorsByOrg(org_name, 1, 30))(equalTo(List(AggregateContributor.test2, AggregateContributor.test))).provideLayer(layer)
      }
    },
    testM("Successfully aggregated two contributors which contribute to different repos") {
      checkM(Gen.anyString) { org_name =>

        val repositories = List(OrgRepositories.test, OrgRepositories.test2)

        val contributors = List(Contributor.test2)
        val contributors2 = List(Contributor.test, Contributor.test2)

        val gitHubClientMocks = (CacheService.lifeContentCache ++ CacheService.lifeEtagCache) >+> (
          GitHubClientMock.GetRepositories(equalTo(org_name, 1, 30), value(repositories)) ++
            GitHubClientMock.GetContributors(equalTo(OrgRepositories.test2.contributors_url, 1, 30), value(contributors2)) &&
            GitHubClientMock.GetContributors(equalTo(OrgRepositories.test.contributors_url, 1, 30), value(contributors))
          ).toLayer

        val layer = (Logger.test ++ gitHubClientMocks) >+> GitHubRankService.life

        assertM(GitHubRankService.getSortedContributorsByOrg(org_name, 1, 30))(equalTo(List(AggregateContributor.test3, AggregateContributor.test))).provideLayer(layer)
      }
    },
    testM("Successfully retrieve empty list where no repos found") {
      checkM(Gen.anyString) { org_name =>

        val repositories = Nil

        val gitHubClientMocks = (CacheService.lifeContentCache ++ CacheService.lifeEtagCache) >+>  GitHubClientMock.GetRepositories(equalTo(org_name, 1, 30), value(repositories)).toLayer

        val layer = (Logger.test ++ gitHubClientMocks) >+> GitHubRankService.life

        assertM(GitHubRankService.getSortedContributorsByOrg(org_name, 1, 30))(equalTo(Nil)).provideLayer(layer)
      }
    },
    testM("Successfully retrieve empty list where no contributors found") {
      checkM(Gen.anyString) { org_name =>

        val repositories = List(OrgRepositories.test)

        val contributors = Nil

        val gitHubClientMocks = (CacheService.lifeContentCache ++ CacheService.lifeEtagCache) >+> (
          GitHubClientMock.GetRepositories(equalTo(org_name, 1, 30), value(repositories)) ++
            GitHubClientMock.GetContributors(equalTo(OrgRepositories.test.contributors_url, 1, 30), value(contributors))
          ).toLayer
        val layer = (Logger.test ++ gitHubClientMocks) >+> GitHubRankService.life

        assertM(GitHubRankService.getSortedContributorsByOrg(org_name, 1, 30))(equalTo(Nil)).provideLayer(layer)
      }
    },
    testM("return count of contributors exactly equals page value") {
      checkM(Gen.anyString) { org_name =>

        val repositories = List(OrgRepositories.test, OrgRepositories.test2)

        val contributors1 = List(Contributor.test2)
        val contributors2 = List(Contributor.test, Contributor.test4)

        val gitHubClientMocks = (CacheService.lifeContentCache ++ CacheService.lifeEtagCache) >+> (
          GitHubClientMock.GetRepositories(equalTo(org_name, 1, 2), value(repositories)) ++
            GitHubClientMock.GetContributors(equalTo(OrgRepositories.test.contributors_url, 1, 2), value(contributors1)) &&
            GitHubClientMock.GetContributors(equalTo(OrgRepositories.test2.contributors_url, 1, 2), value(contributors2))
          ).toLayer
        val layer = (Logger.test ++ gitHubClientMocks) >+> GitHubRankService.life

        assertM(GitHubRankService.getSortedContributorsByOrg(org_name, 1, 2))(equalTo(List(AggregateContributor.test4, AggregateContributor.test2))).provideLayer(layer)
      }
    },
    testM("throw error where no org exist") {
      checkM(Gen.anyString) { org_name =>

        val fail = GitHubResponseError("organisation by " + org_name + " not found")

        val gitHubClientMocks = (CacheService.lifeContentCache ++ CacheService.lifeEtagCache) >+> GitHubClientMock.GetRepositories(equalTo(org_name, 1, 2), failure(fail)).toLayer
        val layer = (Logger.test ++ gitHubClientMocks) >+> GitHubRankService.life

        assertM(GitHubRankService.getSortedContributorsByOrg(org_name, 1, 2).run)(fails(equalTo(fail))).provideLayer(layer)
      }
    }
  )
}