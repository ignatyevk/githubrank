import Dependencies._

scalaVersion := "2.13.1"
name := "githubrank"
version := "0.1.0"

scalacOptions := List(
  "-encoding",
  "utf8",
  "-feature",
  "-unchecked",
  "-language:experimental.macros",
  "-language:higherKinds",
  "-language:postfixOps",
  "-deprecation",
  "-language:_",
  "-Ymacro-annotations"
)

lazy val root = (project in file(".")).settings(name := "githubrank")


libraryDependencies ++= dependencies

testFrameworks := Seq(new TestFramework("zio.test.sbt.ZTestFramework"))

lazy val dependencies: Seq[ModuleID] =
  Cats.all ++
    Http4s.all ++
    Config.all ++
    ZIO.all ++
    Tapir.all ++
    Circe.all ++
    Logging.all ++
    Testing.all